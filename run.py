import psycopg2
import requests
import cx_Oracle


def check_line(line):
    rules = ['Укажите PLU товара: - ', 'Укажите PLU товара (пример): - ']
    for rule in rules:
        if rule in line:
            return True
    return False


def prepare_line(line):
    data = line.strip().split('","')
    for i, line in enumerate(data):
        data[i] = data[i].replace('"', '')
    return data


def from_remedy(line):
    plu = []
    date = '-'.join(line[1].split()[0].split('.')[::-1])
    sap = line[2][-4:]
    mode = 'GK' if line[5] == 'Дискаунтер' else 'NQ' 
    pos = []
    for rule in ['Укажите PLU товара: - ', 'Укажите PLU товара (пример): - ']:
        if rule in line[4]:
            for pos_rule in ['Укажите номер кассы, на которой возникла проблема:  - ', 'Укажите номер кассы, на которой возникла проблема (пример):  - ']:
                if pos_rule in line[4]:
                    try:
                        pos = list(int(line[4].split(pos_rule)[-1].split()[0]))
                    except:
                        try:
                            pos = line[4].split(pos_rule)[-1].split()[0].replace(' ', '').split(',')
                            for i, j in enumerate(pos):
                                pos[i] = int(pos[i])
                        except:
                            pos = []
            plu_field = line[4].split(rule)[-1]
            for i in plu_field.split():
                i = i.replace('"', '').replace(',', ' ').replace('.', ' ').replace('-', ' ')
                if len(i) >= 4:
                    try:
                        plu.append(int(i))
                    except:
                        for j in i.split(' '):
                            if len(j) >= 4:
                                try:
                                    plu.append(int(j))
                                except:
                                    pass
            return {'sap': sap, 'date': date, 'plu': plu, 'mode': mode, 'pos': pos}
        
    return {'sap': sap, 'date': date, 'plu': plu}


def get_work():
    work = []
    with open('work.csv') as f:
        lines = f.readlines()[1:]
    for line in lines:
        if check_line(line):
            line = prepare_line(line)
            remedy = from_remedy(line)
            for plu in remedy['plu']:
                if remedy['pos']:
                    for pos in remedy['pos']:
                        a = line.copy()
                        a.extend([remedy['sap'], remedy['date'], plu, remedy['mode'], pos])
                        work.append(a)
                else:
                    a = line.copy()
                    a.extend([remedy['sap'], remedy['date'], plu, remedy['mode'], ''])
                    work.append(a)
    return work

def from_store(job):
    if job[-2] == 'GK':
        conn = psycopg2.connect(dbname='postgres', user='gkretail', 
                            password='gkretail', host='bo-' + job[-5], connect_timeout=3)
        cursor = conn.cursor()
        cursor.execute("""
                SELECT DISTINCT
                gk_bonkopf.workstation_id,
                xrg_transport_module.timestamp,
                substr(substring(elem FROM 'ean...............'), 6, 15) ean_UTM,
                case when
                right(substr(substring(elem FROM 'barcode........................................................................'), 9, 70), 1) = '"'
                then
                substr(substring(elem FROM 'barcode........................................................................'), 9, 70)
                else
                substr(substring(elem FROM 'barcode.................................................................................................................................................................'), 9, 152)
                end barcode_UTM,
                error_description
                FROM xrg_transport_module
                LEFT JOIN LATERAL unnest(
                    string_to_array(xrg_transport_module.transaction, E'<Bottle ')) WITH ORDINALITY AS a(elem, nr) ON TRUE
                INNER JOIN gk_bonkopf
                    ON xrg_transport_module.id_bsngp = gk_bonkopf.id_bsngp AND xrg_transport_module.bon_seq_id = gk_bonkopf.bon_seq_id
                INNER JOIN gk_bonposition ON xrg_transport_module.bon_seq_id = gk_bonposition.bon_seq_id
                                            AND gk_bonposition.ean = substr(substring(elem FROM 'ean...............'), 6, 15)
                LEFT JOIN xrg_item ON xrg_item.item_id = gk_bonposition.artnr
                LEFT JOIN xrg_item_mrp ON xrg_item_mrp.id_itm = xrg_item.item_id

                WHERE
                -- исключаем заголовок <?xml version="1.0" encoding="UTF-8"...
                a.nr != 1
                AND sign IS NULL
                AND timestamp < date '""" + job[-4] + """' + 1 
                and timestamp > date '""" + job[-4] + """' - 3
                ORDER BY timestamp desc
        """)
        records = cursor.fetchall()
        cursor.close()
        if records:
            if job[-1]:
                for r in records:
                    if r[0] == job[-1]:
                        job.extend(r)
                        job.append('')
                        job[15] = job[15][1:-1]
                        job[13] = str(job[13]).split()[0]
                        return job
                job.extend(['','','','','', ''])
                return job
            else:
                job.extend(records[0])
                job.append('')
                job[13] = str(job[13]).split()[0]
                return job
        else:
            job.extend(['','','','','', ''])
            return job
    elif job[-2] == 'NQ':
        conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@ORA-NS:1521/ORCL", encoding="UTF-8", nencoding="UTF-8")
        cur = conn.cursor()
        cur.execute("""select nickname from sdd.department where id_department in (select id_department from sdd.department_ext where ext_string = '""" + job[7] + """')""")
        response = cur.fetchone()
        conn.close()
        sap = str(response[0])
        conn = cx_Oracle.connect("MONITORING/1AAA2BB3D@ORA-{}:1521/ORCL".format(sap), encoding="UTF-8", nencoding="UTF-8")
        cur = conn.cursor()
        cur.execute("""
                        select *
                        from (select 
                            last_value(t.id_paydesk) over(partition by t.id_paydesk, e.ext_string order by t.date_time RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as paydesk, -- касса
                            to_char(last_value(t.date_time) over(partition by t.id_paydesk, e.ext_string order by t.date_time RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), 'DD.MM.YYYY HH24:MI:SS') as dt, -- время ошибки
                            ee.ext_string as ean, -- EAN (ШК)
                            e.ext_string as alc_rar, -- АМ
                            last_value(d.ext_string) over(partition by t.id_paydesk, e.ext_string order by t.date_time RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as lasterr, -- текст ошибки
                            d.ext_number as nErr, -- код ошибки
                            last_value(t.id_header) over(partition by t.id_paydesk, e.ext_string order by t.date_time RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as head, -- номер чека
                            d.ext_name as err, -- тип ошибки
                            l.id_plu as plu, -- PLU
                            substr(cli.ext_string, 7, 4) as sap -- SAP
                        from cash.checkheader_ext d
                        join cash.checkheader t
                            on t.ID_department = d.id_department
                        and t.id_paydesk = d.id_paydesk
                        and t.id_header = d.id_header
                        and t.date_time > trunc(TO_DATE('""" + job[8] + """', 'YYYY-MM-DD')) - 3
                        and t.date_time < trunc(TO_DATE('""" + job[8] + """', 'YYYY-MM-DD')) + 1
                        left join cash.checkdetail l
                            on l.ID_department = t.id_department
                        and l.id_paydesk = t.id_paydesk
                        and l.id_header = t.id_header
                        join cash.checkdetail_ext e
                            on l.ID_department = e.id_department
                        and l.id_paydesk = e.id_paydesk
                        and l.id_header = e.id_header
                        and l.id_detail = e.id_detail
                        and e.ext_name like 'ALC_RAR'
                        join cash.checkdetail_ext ee
                            on l.ID_department = ee.id_department
                        and l.id_paydesk = ee.id_paydesk
                        and l.id_header = ee.id_header
                        and l.id_detail = ee.id_detail
                        and ee.ext_name like 'EAN'
                        join sdd.client_ext cli
                            on cli.ext_name = 'SAPCODE'
                        and cli.id_company = (select dep.id_company
                                                from sdd.department dep
                                                where dep.is_host = 1)
                        and cli.id_client = (select dep.id_client
                                                from sdd.department dep
                                                where dep.is_host = 1)
                        where d.ext_name in
                            ('EGAIS_ERROR_0001', 'AGENT_MARKCHECK_ERROR_0001')
                        and l.id_detail = e.id_detail
                        and l.qty = 0
                        order by dt desc)
                group by paydesk, dt, ean, alc_rar, nErr, lasterr,  head, err, plu, sap
                order by dt desc
        """)
        records = cur.fetchall()
        conn.close()
        if records:
            if job[-1]:
                for r in records:
                    if r[0] == job[-1]:
                        job.extend(r[:6])
                        job[13] = '-'.join(str(job[13]).split()[0].split('.')[::-1])
                        return job
                job.extend(['','','','','', ''])
              
            else:
                job.extend(records[0][:6])
    
        else:
            job.extend(['','','','','', ''])
        job[13] = '-'.join(str(job[13]).split()[0].split('.')[::-1])
        return job
        

def from_bacchus(job):
    if job[-3]:
        # job[-3] = str(job[-3])[1:-1]
        # print(job[7])
        # print(job[-3])
        # print(str(job[-5]).split()[0])
        # input(job)
        bacchus = cx_Oracle.connect('BAHU_HUBDB_PROD_X5_00/BAHU_HUBDB_PROD_X5_00-password@BACCHUS-HUB01-DB:1521/BACCHUS', encoding = "UTF-8")
        cursor = bacchus.cursor()
        cursor.execute("""
            select * from (
            select 
            coalesce(cart.cart_name, '----------') as Name
            , to_char(bmon.al_created, 'DD.MM.YYYY HH24:MI:SS') as Dt
            , bmon.bmrp_status_code as ErrorCode
            , bamc.BAMC_STATUS as AMC_STATUS
            , bsamc.ROW_STATUS as STOCK_STATUS
            , efb.EFB_F2REGID as FormB
            , bmov_t.BMOV_DESCR as FB_Movement
            , bmon.codv_code as SAP_code
            , bmon.bmrp_barcode as AM
            , ROW_NUMBER() OVER(PARTITION BY bmon.bmrp_barcode ORDER BY bmon.AL_ID DESC) rn
            ,case when bmon.cart_id is null                                then 'не указан cart_id'
                when bmon.cart_id is not null and cart.cart_code is null then 'нет связи с C_ARTICLE'
                else                     coalesce(cart.cart_code,             'нет cart_id')
                end as PLU
        from b_mon_rp bmon
            left join c_articles cart on cart.cart_id = bmon.cart_id
            left join B_AMC bamc ON bamc.BAMC_AMC = bmon.bmrp_barcode
            left join B_STOCKENTRY_AMC bsamc ON bsamc.BAMC_LID = bamc.BAMC_LID and bmon.CODV_ID = bsamc.CODV_ID
            left join B_STOCKENTRY bstk ON bstk.BSTE_ID = bsamc.BSTE_ID
            left join E_FB efb on efb.EFB_ID = bstk.EFB_ID
            left join (select bmov.*, ROW_NUMBER() OVER(PARTITION BY bmov.EFB_ID ORDER BY bmov.bmov_id DESC) rn from B_MOVEMENTS bmov) bmov on rn = 1 and bmov.EFB_ID = bstk.EFB_ID
            left join B_MOVEMENT_TYPES bmov_t ON bmov_t.BMOV_TYPE = bmov.BMOV_TYPE
        where 1=1
            and bmon.codv_code = to_char('""" + job[7] + """') --SAP code
            and bmon.bmrp_barcode = '""" + job[12] + """' --марка
            and bmon.AL_CREATED >= date'""" + str(job[10]) + """' --начальная дата
            and coalesce(bmon.bmrp_status_code,'8000') != '8000'
            ) WHERE rn = 1
        """)
        records = cursor.fetchone()
        cursor.close()
        if records:
            job.extend(records[:7])
        else:
            job.extend(['', '', '', '', '', '', ''])
    else:
        job.extend(['', '', '', '', '', '', ''])
    return job


def from_egais(job):
    if len(job[12]) == 150:
        request = requests.get('https://mobileapi.fsrar.ru/api/marklong?pdf417={}'.format(job[12]), verify=False)
        request = request.json()
        msg = request['msg']
        job.append(msg)
        job.append(request)
    else:
        job.extend(['', ''])
    return(job)


if __name__ == "__main__":
    with open('result.csv', 'w') as f:
        f.write('ID инцидента;Дата создания;SAP ID;Содержание;Примечания;Тип объекта;Назначено группе;SAP;PLU;GK/NQ: касса;GK/NQ: время ошибки;AGK/NQ: ШКM;GK/NQ: АМ;GK/NQ: ошибка;GK/NQ: код ошибки;Бахус: наименование АП;Бахус: время ошибки;Бахус: код ошибки;Бахус: статус B_AMC;Бахус: статус STOCK_AMC;Бахус: справка B;Бахус: движение справки;ЕГАИС: инфо;ЕГАИС: полный ответ\n')
    jobs = get_work()
    for i, job in enumerate(jobs):
        jobs[i] = from_store(jobs[i])
        jobs[i].pop(8)
        jobs[i].pop(9)
        jobs[i].pop(9)
        jobs[i][12] = jobs[i][12].replace('"','')
        jobs[i] = from_bacchus(jobs[i])
        jobs[i] = from_egais(jobs[i])
        jobs[i] = [str(j) for j in jobs[i]]
        with open('result.csv', 'a') as f:
            f.write(';'.join(jobs[i]) + '\n')




